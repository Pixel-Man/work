import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      name: 'Home',
      path: '/',
      meta: { layout: 'main' },
      component: () => import('./views/HomePage'),
    },
    {
      name: 'Login',
      path: '/login',
      meta: { layout: 'empty' },
      component: () => import('./views/EmptyPage'),
    },
  ],
});
