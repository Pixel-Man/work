import GoogleSpreadsheet from 'google-spreadsheet';

export default class GoogleSheetsApi {
  constructor() {
    this.doc = new GoogleSpreadsheet('1vs8QlxYgAY1Yiho7Ab3WKep47TdMa58ciXzyRd6L11o');

    this.initApi();
  }

  async initApi() {
    await this.doc.useServiceAccountAuth({
      client_email: process.env.GOOGLE_SERVICE_ACCOUNT_EMAIL,
      private_key: process.env.GOOGLE_PRIVATE_KEY,
    });
  }
}
