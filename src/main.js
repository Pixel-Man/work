import 'normalize.css';
import './assets/fonts/fonts.sass';
import './assets/common/index.sass';
import 'swiper/swiper-bundle.css';
import Toast from 'vue-toastification';
import 'vue-toastification/dist/index.css';

import Vue from 'vue';

import VModal from 'vue-js-modal';
import VueMask from 'v-mask';
import Vuex from 'vuex';

import App from './App.vue';
import './registerServiceWorker';
import router from '@/router';

Vue.use(VModal, {
  dynamic: true,
});
Vue.use(Toast, {
  transition: 'Vue-Toastification__bounce',
  maxToasts: 20,
  newestOnTop: true,
});
Vue.use(VueMask);
Vue.use(Vuex);

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
