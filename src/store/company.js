export default [
  {
    id: 1,
    title: 'Company 1',
    shotDesc: `
      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
      sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
      Tristique magna sit amet purus gravida quis blandit turpis cursus.
    `,
    photos: [
      'company_1-1.jpg',
      'company_1-2.jpg',
      'company_1-3.jpg',
      'company_1-4.jpg',
      'company_1-4.jpg',
      'company_1-4.jpg',
    ],
    fullDesc: `
      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
      sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
      Tristique magna sit amet purus gravida quis blandit turpis cursus.
      Lectus urna duis convallis convallis tellus id interdum velit laoreet.
      Vel pretium lectus quam id leo in vitae turpis. Mauris cursus mattis molestie a iaculis at erat.
      Mi in nulla posuere sollicitudin aliquam. Bibendum neque egestas congue quisque egestas diam in arcu cursus.
      Libero id faucibus nisl tincidunt eget nullam non nisi. Ullamcorper eget nulla facilisi etiam dignissim.
      Morbi tristique senectus et netus. Scelerisque purus semper eget duis at tellus at.
      Id faucibus nisl tincidunt eget nullam. Elit scelerisque mauris pellentesque pulvinar pellentesque.
      Aliquet porttitor lacus luctus accumsan tortor. Elit eget gravida cum sociis natoque penatibus.
    `,
  }, {
    id: 2,
    title: 'Company 2',
    shotDesc: `
      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
      sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
      Tristique magna sit amet purus gravida quis blandit turpis cursus.
    `,
    photos: [
      'company_1-2.jpg',
      'company_1-1.jpg',
      'company_1-3.jpg',
      'company_1-4.jpg',
      'company_1-4.jpg',
      'company_1-4.jpg',
    ],
    fullDesc: `
      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
      sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
      Tristique magna sit amet purus gravida quis blandit turpis cursus.
      Lectus urna duis convallis convallis tellus id interdum velit laoreet.
      Vel pretium lectus quam id leo in vitae turpis. Mauris cursus mattis molestie a iaculis at erat.
      Mi in nulla posuere sollicitudin aliquam. Bibendum neque egestas congue quisque egestas diam in arcu cursus.
      Libero id faucibus nisl tincidunt eget nullam non nisi. Ullamcorper eget nulla facilisi etiam dignissim.
      Morbi tristique senectus et netus. Scelerisque purus semper eget duis at tellus at.
      Id faucibus nisl tincidunt eget nullam. Elit scelerisque mauris pellentesque pulvinar pellentesque.
      Aliquet porttitor lacus luctus accumsan tortor. Elit eget gravida cum sociis natoque penatibus.
    `,
  }, {
    id: 3,
    title: 'Company 3',
    shotDesc: `
      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
      sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
      Tristique magna sit amet purus gravida quis blandit turpis cursus.
    `,
    photos: [
      'company_1-3.jpg',
      'company_1-1.jpg',
      'company_1-2.jpg',
      'company_1-4.jpg',
      'company_1-4.jpg',
      'company_1-4.jpg',
    ],
    fullDesc: `
      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
      sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
      Tristique magna sit amet purus gravida quis blandit turpis cursus.
      Lectus urna duis convallis convallis tellus id interdum velit laoreet.
      Vel pretium lectus quam id leo in vitae turpis. Mauris cursus mattis molestie a iaculis at erat.
      Mi in nulla posuere sollicitudin aliquam. Bibendum neque egestas congue quisque egestas diam in arcu cursus.
      Libero id faucibus nisl tincidunt eget nullam non nisi. Ullamcorper eget nulla facilisi etiam dignissim.
      Morbi tristique senectus et netus. Scelerisque purus semper eget duis at tellus at.
      Id faucibus nisl tincidunt eget nullam. Elit scelerisque mauris pellentesque pulvinar pellentesque.
      Aliquet porttitor lacus luctus accumsan tortor. Elit eget gravida cum sociis natoque penatibus.
    `,
  },
];
